const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    const users = UserService.getAll();
    res.status(200).json(users);
    next();
});

router.get('/:id', (req, res, next) => {
    const user = getUser(req, res, 'User is not found');
    if (user) {
        res.status(200).json(user);
    }
    next();
});

router.post('/', createUserValid,  (req, res, next) => {
    if (!res.err) {
        const user = UserService.create(req.body);
        res.status(200).json(user);
    }
    next();
});

router.put('/:id', updateUserValid, (req, res, next) => {
    const user = getUser(req, res, 'Unable to update nonexistent user');
    if (user && !res.err) {
        UserService.update(req.params.id, req.body);
        res.status(200).json(user);
    }
    next();
});

router.delete('/:id', (req, res, next) => {
    const user = getUser(req, res, 'Unable to delete nonexistent user')
    if (user) {
        UserService.delete(req.params.id);
        res.status(200).json(user);
    }
    next();
});

function getUser(req, res, errMessage) {
    const id = req.params.id;
    const user = UserService.search({id});
    if (!user) {
        res.status(404).json({
            error: true,
            message: errMessage
        });
        return null;
    }
    return user;
}

module.exports = router;