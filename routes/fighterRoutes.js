const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    const fighter = FighterService.getAll();
    res.status(200).json(fighter);
    next();
});

router.get('/:id', (req, res, next) => {
    const fighter = getFighter(req, res, 'User is not found');
    if (fighter) {
        res.status(200).json(fighter);
    }
    next();
});

router.post('/', createFighterValid,  (req, res, next) => {
    if (!res.err) {
        const fighter = FighterService.create(req.body);
        res.status(200).json(fighter);
    }
    next();
});

router.put('/:id', updateFighterValid, (req, res, next) => {
    const fighter = getFighter(req, res, 'Unable to update nonexistent fighter');
    if (fighter && !res.err) {
        FighterService.update(req.params.id, req.body);
        res.status(200).json(fighter);
    }
    next();
});

router.delete('/:id', (req, res, next) => {
    const fighter = getFighter(req, res, 'Unable to delete nonexistent fighter')
    if (fighter) {
        FighterService.delete(req.params.id);
        res.status(200).json(fighter);
    }
    next();
});

function getFighter(req, res, errMessage) {
    const id = req.params.id;
    const fighter = FighterService.search({id});
    if (!fighter) {
        res.status(404).json({
            error: true,
            message: errMessage
        });
        return null;
    }
    return fighter;
}

module.exports = router;