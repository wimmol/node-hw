const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAll() {
        const item = FighterRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    create(user) {
        return FighterRepository.create(user);
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();