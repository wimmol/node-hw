const { userModel } = require('../models/user');

const [id, ...template] = Object.keys(userModel);

const validPaterns = {
    firstName: /^[a-zA-Z]+$/,
    lastName: /^[a-zA-Z]+$/,
    email: /^[\w\.]+@gmail\.com$/i,
    phoneNumber: /^\+380[0-9]{9}$/,
    password: /.../
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const user = req.body;
    try {
        checkId(user);
        checkPropertiesAvailability(user);
        checkPropertiesExcess(user);
        checkPropertiesValid(user)
    }
    catch (err) {
        res.status(400).json({
            error: true,
            message: err
        });
        res.err = err;
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const user = req.body;
    try {
        checkId(user);
        checkPropertiesExcess(user);
        checkPropertiesValid(user)
    }
    catch (err) {
        res.status(400).json({
            error: true,
            message: err
        });
        res.err = err;
    }
    next();
}

function checkPropertiesValid(user) {
    for (let key in user) {
        if (!validPaterns[key].test(user[key])) {
            throw `Field ${key} is not valid`;
        }
    }
}

function checkId(user) {
    if (user.hasOwnProperty('id')) {
        throw 'You cant set id';
    }
}

function checkPropertiesAvailability(user) {
    template.forEach(key => {
        if (!user.hasOwnProperty(key)) {
            throw `You should set ${key}`;
        }
    })
}

function checkPropertiesExcess(user){
    Object.keys(user).forEach(key => {
        if (!template.includes(key)) {
            throw `You sent extra field ${key}`;
        }
    });
}



exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;