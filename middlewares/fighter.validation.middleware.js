const { fighterModel } = require('../models/fighter');

const [id, ...template] = Object.keys(fighterModel);

const validPaterns = {
    name: /^[a-zA-Z]+$/,
    health: /^[0-9]+$/,
    power: /^[0-9]+$/,
    defense: /^[1-9]$|^10$/, // 1 to 10
}
const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const fighter = req.body;
    try {
        checkId(fighter);
        checkPropertiesAvailability(fighter);
        checkPropertiesExcess(fighter);
        checkPropertiesValid(fighter)
    }
    catch (err) {
        res.status(400).json({
            error: true,
            message: err
        });
        res.err = err;
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const fighter = req.body;
    try {
        checkId(fighter);
        checkPropertiesExcess(fighter);
        checkPropertiesValid(fighter);
    }
    catch (err) {
        res.status(400).json({
            error: true,
            message: err
        });
        res.err = err;
    }
    next();
}

function checkPropertiesValid(fighter) {
    for (let key in fighter) {
        if (!validPaterns[key].test(fighter[key])) {
            throw `Field ${key} is not valid`;
        }
    }
}

function checkId(fighter) {
    if (fighter.hasOwnProperty('id')) {
        throw 'You cant set id';
    }
}

function checkPropertiesAvailability(fighter) {
    template.forEach(key => {
        if (!fighter.hasOwnProperty(key)) {
            throw `You should set ${key}`;
        }
    })
}

function checkPropertiesExcess(fighter){
    Object.keys(fighter).forEach(key => {
        if (!template.includes(key)) {
            throw `You sent extra field ${key}`;
        }
    });
}


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;